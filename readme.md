On windows,  I generated a mvm project :

```
mvn archetype:generate "-DarchetypeGroupId=uk.ac.gate" "-DarchetypeArtifactId=gate-pr-archetype" "-DarchetypeVersion=8.6"
```

I added in the pom.xml file this line :

```xml
<packaging>jar</packaging>
```

To build the project, I did `mvm package` (sometime I did `mvm install`, I don't know exactly the differences : sorry for my Java level)

Gate didn't want to add my plugging (when I tried to add it in the Maven way) but it works when I added the creole.xml in the root file.

Then Gate added it but Gate cannot see the accessor.
