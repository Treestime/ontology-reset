package com.baulders;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Document;
import gate.Resource;
import gate.creole.AbstractLanguageAnalyser;
import gate.creole.ExecutionException;
import gate.creole.ExecutionInterruptedException;
import gate.creole.ResourceInstantiationException;
import gate.creole.metadata.*;
import gate.creole.ontology.Ontology;

import org.apache.log4j.Logger;

@CreoleResource(
    name = "Ontology Reset",
    comment = "Reset your ontology (clean and reload)")
public class OntologyReset extends AbstractLanguageAnalyser {

  private static final Logger log = Logger.getLogger(OntologyReset.class);

  /**
   * The ontology to reset (LR)
   */
  private Ontology ontology = null;

  /**
   * The file to load (owl)
   */
  //private String file;

  public Ontology getOntology() {
    return this.ontology;
  }


  /*@RunTime
  @CreoleParameter(comment = "The file to load (owl)")
  public void setFile(String file) {
    this.file = file;
  }*/
  @RunTime
  @CreoleParameter(comment = "The ontology to reset (LR)")
  public void setOntology(Ontology ontology) {
    this.ontology = ontology;
  }

  /**
   * Initialize this Ontology Reset.
   * @return this resource.
   * @throws ResourceInstantiationException if an error occurs during init.
   */
  public Resource init() throws ResourceInstantiationException {
    return this;
  }

  /**
   * Execute this Ontology Reset over the current document.
   * @throws ExecutionException if an error occurs during processing.
   */
  public void execute() throws ExecutionException {
    // check the interrupt flag before we start - in a long-running PR it is
    // good practice to check this flag at appropriate key points in the
    // execution, to allow the user to interrupt processing if it takes too
    // long.
    /*if(isInterrupted()) {
      throw new ExecutionInterruptedException("Execution of Ontology Reset has been interrupted!");
    }
    interrupted = false;
  
    ontology.cleanOntology(); */
    log.debug("Ontology is cleaned");
  }

}

